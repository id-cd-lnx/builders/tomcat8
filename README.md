[![pipeline status](https://gitlab.ethz.ch/id-cd-lnx/builders/tomcat8/badges/master/pipeline.svg)](https://gitlab.ethz.ch/id-cd-lnx/builders/tomcat8/-/commits/master)

# tomcat8

Determine latest version, fetch and build tomcat8 RPM. Packages avaiable here: https://idinstallprd.ethz.ch/repos/rpmbuilder/tomcat8/
